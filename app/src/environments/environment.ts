export const environment = {
    "name": "dev",
    "properties": {
        "production": false,
        "ssdURL": "http://localhost:8081/api/",
        "tenantName": "neutrinos-pre-sales",
        "appName": "emirates-poc",
        "namespace": "com.neutrinos-pre-sales.emirates-poc",
        "googleMapKey": "AIzaSyCSTnVwijjv0CFRA4MEeS-H6PAQc87LEoU",
        "useDefaultExceptionUI": true,
        "isIDSEnabled": "true",
        "webAppMountpoint": "web"
    }
}