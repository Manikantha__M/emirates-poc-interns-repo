/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core'
import { MediaMatcher } from '@angular/cdk/layout';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { NeutrinosOAuthClientService } from 'neutrinos-oauth-client';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-home',
    templateUrl: './home.template.html'
})

export class homeComponent extends NBaseComponent implements OnInit, OnDestroy {

    @ViewChild('drawer', { static: false }) drawer: any;
    public isHandset: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(map((result: BreakpointState) => result.matches));
    public isMobile: boolean;
    mobileQuery: MediaQueryList;
    private authSubscribe;
    public selectedItem: string = '';
    private _mobileQueryListener: () => void;
    public userInfo = this.neutrinosOAuthClientService.userInfo;
    public teams = this.neutrinosOAuthClientService.userInfo.teams
    public toolbarItems = {
        knowledgeCenter: "http://docs1.neutrinos.co/",
        console: "https://console.neutrinos.co/",
        store: "https://store.neutrinos.co/"
    }

    constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public neutrinosOAuthClientService: NeutrinosOAuthClientService, private breakpointObserver: BreakpointObserver) {
        super();
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    }

    ngOnInit() {
        this.authSubscribe = this.neutrinosOAuthClientService.authState().subscribe(state => {
            // Perform user friendly action when session becomes invalid
        });
    }

    async logoutUser() {
        await this.neutrinosOAuthClientService.logout();
    }

    goToLink(url: string) {
        window.open(url, "_blank");
    }

    closeSideNav() {
        if (this.drawer._mode == 'over') {
            this.drawer.close();
        }
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
        if (this.authSubscribe) {
            this.authSubscribe.unsubscribe();
        }
    }
}