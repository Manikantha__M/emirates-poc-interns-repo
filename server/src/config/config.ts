export default {
  logger: {
    level: 'debug',
    transport: ['file', 'console'],
    logFile: 'logs/console.log',
    exceptionFile: 'logs/exception.log'
  },
  middlewares: {
    pre: [{ __ssdGlobalMiddlewares__: 'sd_Tp9jNbUvUZ1kJnAn' }],
    post: [],
    sequences: {}
  }
};
